﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel;

namespace ReadExcel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        DataSet ds;
        DataTable dataTable = new DataTable();
        DataTable resultDataTable = new DataTable();
        DataTable resultDT = new DataTable();
        string sqlConnStr = Properties.Settings.Default.connStr;

        private void btnOpen_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    MessageBox.Show("Files found: " + files.Length.ToString(), "Message");
                    lblBrojFileova.Text = files.Length.ToString();

                    foreach (var file in files)
                    {
                        //MessageBox.Show("Files found: " + file, "Message");
                        FileStream fs = File.Open(file, FileMode.Open, FileAccess.Read);
                        IExcelDataReader reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
                        reader.IsFirstRowAsColumnNames = true;
                        ds = reader.AsDataSet();
                        DataTable dt = ds.Tables[0];

                        //provjera koliko ima razlicitih partija u excel - fileu
                        resultDT = RemoveEmptyRowsFromDataTable(dt);
                        int relationshipCount = resultDT
                                        .AsEnumerable()
                                        .Select(r => r.Field<string>("Partija"))
                                        .Distinct()
                                        .Count();
                        if (relationshipCount == 1)
                        {
                            dataTable.Merge(resultDT);
                            //MessageBox.Show("U excel file-u: " + Path.GetFileName(file) + " ima vise RAZLICITIH partija", "Message");
                        }
                        else
                        {
                            MessageBox.Show("U excel file-u: " + Path.GetFileName(file) + " ima vise RAZLICITIH partija", "Message");
                        }

                        
                    }

                    // uklanjanje praznih redova
                    resultDataTable = RemoveEmptyRowsFromDataTable(dataTable);

                    // dodajem Identity column
                    AddAutoIncrementColumn(resultDataTable);

                    
                    //provjera tipa colona   dgf
                    //foreach (DataColumn column in resultDataTable.Columns)
                    //{
                    //    MessageBox.Show(column + " tip "+ column.DataType.ToString());
                    //}

                    // kontrole shodno pravilima koje treba definisati ili kreirati xsd schema

                    // popunjavanje Data grid view na formi
                    dataGridView1.DataSource = resultDataTable;   
                }
            }
        }

        private void AddAutoIncrementColumn(DataTable resultDataTable)
        {
            DataColumn ID = new DataColumn();
            ID.DataType = System.Type.GetType("System.Int32");
            
            ID.AutoIncrement = true;
            ID.AutoIncrementSeed = 0;
            ID.AutoIncrementStep = 1;
            resultDataTable.Columns.Add(ID);
            resultDataTable.Columns[13].ColumnName = "id";
            resultDataTable.Columns[13].SetOrdinal(0);
            resultDataTable.AcceptChanges();
            int index = -1;
            foreach (DataRow row in resultDataTable.Rows)
            {
                row.SetField(ID, ++index);
            }
        }

        DataTable RemoveEmptyRowsFromDataTable(DataTable dt)
        {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                if (dt.Rows[i][1] == DBNull.Value)
                    dt.Rows[i].Delete();
            }
            dt.AcceptChanges();
            return dt;
        }

        private void btnUpisi_Click(object sender, EventArgs e)
        {
            btnUpisi.Enabled = false;
            using (SqlConnection connection= new SqlConnection(sqlConnStr))
            {
                connection.Open();
                using (var sqlBulk = new SqlBulkCopy(sqlConnStr))
                {
                    sqlBulk.DestinationTableName = "Projekcija_tokova_gotovine";
                    sqlBulk.WriteToServer(resultDataTable);
                }
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
